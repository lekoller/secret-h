const jogo = document.getElementById('game');
const listOfPlayer = document.createElement('ul');
const startButton = document.createElement('button');
const resortButton = document.createElement('button');
const placeForTheList = document.getElementById('listPlace');
const asideObject = document.getElementById('lateral');
const dialogObject = document.getElementById('dialogPlace');
const drawLawArea = document.getElementById('pileOfDrawPlace');
const discardLawArea = document.getElementById('pileOfDiscardPlace');
const board = document.createElement('div');
const dialogBox = document.createElement('div');
const jaButtonGov = document.createElement('button');
const neinButtonGov = document.createElement('button');
const drawPileImg = document.createElement('img');
const discardPileImg = document.createElement('img');
const threeCardsShow = document.createElement('div');
const liberalLawsBoard = document.createElement('div');
const liberalMarkableArea = document.createElement('div');
const fascistLawsBoard = document.createElement('div');
const fascistMarkableArea = document.createElement('div');
liberalLawsBoard.appendChild(liberalMarkableArea);
fascistLawsBoard.appendChild(fascistMarkableArea);

    placeForTheList.appendChild(listOfPlayer);
    listOfPlayer.id = 'theList';

let playersNumber = 5;
let nameField = [];
let arrayOfPlayers = [];
let playersDisplay = [];
let orderedPlayers = [];
let playerFrame = [];
let liberalLaws = [];
let fascistLaws = [];
let deckOfLaws = [];
let pileOfDraw = [];
let pileOfDiscard = [];
let chosenPresident = undefined;
let fascistBoardSize = 1;
let thisTurnCards = [];
let lawCardFrame = [];
let aprovedLiberalLaws = [];
let aprovedFascistLaws = [];
let liberalLawsCount = 0;
let fascistLawsCount = 0;

class Jogador {
    constructor(name) {
        this._name = name;
        this._politics = 'liberal';
        this._role = 'parliamentary';
        this.isDead = false;
    }

    getName() {
        return this._name;
    }
    setName(nome) {
        this._name = nome;
    }

    getPolitics() {
        return this._politics;
    }
    setPolitics(valor) {
        this._politics = valor;
    }

    getRole() {
        return this._role;
    }
    setRole(cargo) {
        this._role = cargo;
    }

    kill() {
        this.isDead = true;
    }
}

const guests = document.createElement('form');
const field = document.createElement('fieldset');
const legnd = document.createElement('legend');
const entry = document.createElement('input');
    jogo.appendChild(guests);
    guests.appendChild(field);
    field.appendChild(legnd);
    field.appendChild(entry);
    entry.type = 'number';
    entry.name = 'numberOfPlayers';
    entry.id = 'numOfPlayers';
    entry.min = '5';
    entry.max = '10';
    entry.placeholder = 'number';
    legnd.innerText = 'Players?';

const generateNamesForm = function() {
    playersNumber = parseInt(entry.value);

    if (playersNumber >= 5 && playersNumber <= 10) {
        const namesForm = document.createElement('form');
        const playersField = document.createElement('fieldset');
        const playersLegend = document.createElement('legend')
        jogo.appendChild(namesForm);
        namesForm.appendChild(playersField);
        namesForm.id = 'formDeNomes';
        playersField.appendChild(playersLegend);
        playersLegend.innerText = 'Names:'

        for (let i = 0; i < playersNumber; i++) {
            nameField[i] = document.createElement('input');
            playersField.appendChild(nameField[i]);
            nameField[i].type = 'text'
            nameField[i].name = `Player ${i + 1} name`;
            nameField[i].id = `name${i + 1}`;
            nameField[i].size = '30';
            nameField[i].maxLength = '50';
            nameField[i].placeholder = `Player number ${i + 1}`;
        }

        jogo.appendChild(startButton);
        startButton.id = 'startGame';
        startButton.innerText = 'Ready!';
        startButton.addEventListener('click', generatePlayersStatus);
    }
}

function generatePlayersStatus() {
    mainGenerator();
    destroyForms();
    generateList();
}

function mainGenerator() {
    const chosen = Math.floor(Math.random() * nameField.length);
    let fascistsCount = 1;

    switch(nameField.length) {
        case 7:
        case 8:
            fascistsCount = 2;
            fascistBoardSize = 2;
            break
        case 9:
        case 10:
            fascistsCount = 3;
            fascistBoardSize = 3;
            break;
        default:
            break;
    }

    for (let i = 0; i < nameField.length; i++) {
        arrayOfPlayers[i] = new Jogador(nameField[i].value)
    }

    arrayOfPlayers[chosen].setPolitics('hitler');

    while (fascistsCount > 0) {
        let fascist = Math.floor(Math.random() * arrayOfPlayers.length);

        if (arrayOfPlayers[fascist].getPolitics() === 'liberal') {
            arrayOfPlayers[fascist].setPolitics('fascist');
            fascistsCount--;
        }
    }

    // console.log(arrayOfPlayers)
}

function destroyForms() {
    const namesForm = document.getElementById('formDeNomes');
    const startButton = document.getElementById('startGame');

    jogo.removeChild(guests);
    jogo.removeChild(setButton);
    jogo.removeChild(namesForm);
    jogo.removeChild(startButton);
}

function generateList() {   

    for (let i = 0; i < arrayOfPlayers.length; i++) {
        playersDisplay[i] = document.createElement('li');
        listOfPlayer.appendChild(playersDisplay[i]);
        playersDisplay[i].innerText = `${arrayOfPlayers[i].getName()}: ${arrayOfPlayers[i].getPolitics()}`;
    }

    asideObject.appendChild(resortButton);
    resortButton.innerText = 'Restart with the same players';
    resortButton.id = 'resort';
    resortButton.addEventListener('click', newListGenerator);
    
    launchGame();
}

function newListGenerator() {
    const liberalClean = aprovedLiberalLaws.length;
    const fascistClean = aprovedFascistLaws.length;
    const resortButton = document.getElementById('resort');

    for (let i = 0; i < arrayOfPlayers.length; i++) {
        listOfPlayer.removeChild(playersDisplay[i]);
    }

    asideObject.removeChild(resortButton);

    pileOfDraw = [];
    pileOfDiscard = [];
    liberalLawsCount = 0;
    fascistLawsCount = 0;

    for (let i = 0; i < liberalClean; i++) {
        aprovedLiberalLaws[i].remove();
    }

    for (let i = 0; i < fascistClean; i++) {
        aprovedFascistLaws[i].remove();
    }

    degenerateRoleCards();
    degeneratePileOfLawCards();
    degenerateLiberalLawsBoard();
    degenerateFascistLawsBoard();
    mainGenerator();
    generateList();
}

const setButton = document.createElement('button');
jogo.appendChild(setButton);
setButton.innerText = 'Set Players...';
setButton.addEventListener('click', generateNamesForm);

function launchGame() {
    reorder();
    pickPresident();
    startLawCards();
    generateBoard();
}

function reorder() {
    for (let i = 0; i < arrayOfPlayers.length; i++) {
        orderedPlayers[i] = arrayOfPlayers[i];
    }

    orderedPlayers.sort((a, b) => { return 0.5 - Math.random() });
}

function pickPresident() {
    chosenPresident = Math.floor(Math.random() * orderedPlayers.length);
    orderedPlayers[chosenPresident].setRole('president');
}

function startLawCards() {
    for (let i = 0; i < 6; i++) {
        liberalLaws[i] = true;
    }
    for (let i = 0; i < 11; i++) {
        fascistLaws[i] = false;
    }

    deckOfLaws = liberalLaws.concat(fascistLaws);
    deckOfLaws.sort((a, b) => { return 0.5 - Math.random() });

    for (let i = 0; i < deckOfLaws.length; i++) {
        if (deckOfLaws[i]) {
            pileOfDraw.push('liberalLaw');
        } else {
            pileOfDraw.push('fascistLaw');
        }
    }
}

function generateBoard() {
    jogo.appendChild(board);
    dialogObject.appendChild(dialogBox);
    board.id = 'boardSpace';
    dialogBox.id = 'dialog';
    dialogBox.innerHTML = '<p>President, choose your chancellor!</p>';

    generateRoleCards();
    generatePileOfLawCards();
    generateLiberalLawsBoard();
    generateFascistLawsBoard();

    liberalMarkableArea.id = 'liberalMarkable';
    fascistMarkableArea.id = `fascistMarkableV${fascistBoardSize}`;
}

function generateRoleCards() {
    for (let i = 0; i < orderedPlayers.length; i++) {
        playerFrame[i] = document.createElement('div');
        playerFrame[i].className = `itsA${orderedPlayers[i].getPolitics()}`;
        playerFrame[i].id = `itsA${orderedPlayers[i].getRole()}`;
        playerFrame[i].title = `${orderedPlayers[i].getName()}, ${orderedPlayers[i].getRole()}`;
        if (orderedPlayers[i].getRole() !== 'president') {
            playerFrame[i].addEventListener('click', callbackChancellor);
        }
        board.appendChild(playerFrame[i]);
    }
}

const reselectChancellor = function() {
    dialogBox.innerHTML = '<p>President, choose your chancellor!</p>';
    refreshGraphics();
}

const callbackChancellor = function() {
    this.id = 'itsAchancellor';
    this.addEventListener('click', reselectChancellor);
    governmentAprovation();
}

const isAproved = function() {
    setChancellor();
    refreshGraphics();
    drawLawCards();
}

const notAproved = function() {
    changePresident();
    dialogBox.innerHTML = '<p>President, choose your chancellor!</p>';
    refreshGraphics();
    neinButtonGov.removeEventListener('click', notAproved);
}

function governmentAprovation() {

    for (let i = 0; i < orderedPlayers.length; i++) {
        if (orderedPlayers[i].getRole() !== 'president') {
            playerFrame[i].removeEventListener('click', callbackChancellor);
        }
    }

    dialogBox.innerHTML = '<p>Is this government aproved?</p>';
    dialogBox.appendChild(jaButtonGov);
    jaButtonGov.innerText = 'Ja!';
    dialogBox.appendChild(neinButtonGov);
    neinButtonGov.innerText = 'Nein...';

    jaButtonGov.addEventListener('click', isAproved)
    neinButtonGov.addEventListener('click', notAproved)
}

// function generateJaNeinButtons() {
//     dialogBox.appendChild(jaButton);
//     jaButton.innerText = 'Ja!';
//     dialogBox.appendChild(neinButton);
//     neinButton.innerText = 'Nein...';
// }

function generatePileOfLawCards() {
    drawLawArea.appendChild(drawPileImg);
    drawPileImg.src = 'images/pileOfDraw.png';
    drawPileImg.title = `${pileOfDraw.length} cards`;

    discardLawArea.appendChild(discardPileImg);
    discardPileImg.src = 'images/pileOfDiscard.png';
    discardPileImg.title = `${pileOfDiscard.length} cards`;
}

function generateFascistLawsBoard() {
    board.appendChild(fascistLawsBoard);
    fascistLawsBoard.id = `fascistBoard${fascistBoardSize}`;
}

function generateLiberalLawsBoard() {
    board.appendChild(liberalLawsBoard);
    liberalLawsBoard.id = 'liberalBoard';
}

function degenerateFascistLawsBoard() {
    board.removeChild(fascistLawsBoard);
}

function degenerateLiberalLawsBoard() {
    board.removeChild(liberalLawsBoard);
}

function refreshGraphics() {
    degenerateRoleCards();
    generateRoleCards();
    degeneratePileOfLawCards();
    generatePileOfLawCards();
    degenerateLiberalLawsBoard();
    generateLiberalLawsBoard();
    degenerateFascistLawsBoard();
    generateFascistLawsBoard();
}

function degenerateRoleCards() {
    for (let i = 0; i < playerFrame.length; i++) {
        board.removeChild(playerFrame[i]);
    }
}

function degeneratePileOfLawCards() {
    drawLawArea.removeChild(drawPileImg);
    discardLawArea.appendChild(discardPileImg);
}

function setChancellor() {
    for (let i = 0; i < playerFrame.length; i++) {
        if (playerFrame[i].id === 'itsAchancellor') {
            orderedPlayers[i].setRole('chancellor');
            break;
        }
    }
}

function cancelChancellor() {
    for (let i = 0; i < orderedPlayers.length; i++) {
        if (orderedPlayers[i].getRole() === 'chancellor') {
            orderedPlayers[i].setRole('parliamentary');
            break;
        }
    }
}

function changePresident() {
    orderedPlayers[chosenPresident].setRole('parliamentary');
    if (chosenPresident < orderedPlayers.length - 1){
        chosenPresident++;
    } else {
        chosenPresident = 0;
    }
    orderedPlayers[chosenPresident].setRole('president');
}

function drawLawCards() {
    
    if (pileOfDraw.length >= 3) {
        jogo.appendChild(threeCardsShow);
        threeCardsShow.id = 'showCardsArea';
        dialogBox.innerHTML = '<p>President, choose one policy to <strong>REJECT</strong>.</p>'

        for (let i = 0; i < 3; i++) {
            const lawCard = pileOfDraw.pop();
            thisTurnCards.push(lawCard);
            lawCardFrame[i] = document.createElement('div');
            threeCardsShow.appendChild(lawCardFrame[i]);
            lawCardFrame[i].id = `card${i}`;
            lawCardFrame[i].className = lawCard;
            lawCardFrame[i].addEventListener('click', rejectLaw);
        }
        refreshGraphics();
    } else {
        pileOfDraw = pileOfDiscard.sort((a, b) => { return 0.5 - Math.random() });
        pileOfDiscard = [];
        refreshGraphics();
        drawLawCards();
    }
}

function rejectLaw() {
    dialogBox.innerHTML = '<p>Chancellor, choose one policy to <strong>APROVE</strong>.</p>'
    threeCardsShow.removeChild(this)
    for (let i = 0; i < thisTurnCards.length; i++) {
        lawCardFrame[i].removeEventListener('click', rejectLaw);
        lawCardFrame[i].addEventListener('click', aproveLaw);
    }
}

function aproveLaw() {
    if (this.className === 'liberalLaw'){
        aprovedLiberalLaws[liberalLawsCount] = document.createElement('div');
        liberalMarkableArea.appendChild(aprovedLiberalLaws[liberalLawsCount]);
        aprovedLiberalLaws[liberalLawsCount].className = 'markLiberal';
        liberalLawsCount++;
    } else {
        aprovedFascistLaws[fascistLawsCount] = document.createElement('div');
        fascistMarkableArea.appendChild(aprovedFascistLaws[fascistLawsCount]);
        aprovedFascistLaws[fascistLawsCount].className = `markFascistV${fascistBoardSize}`;
        fascistLawsCount++;
    }

    for (let i = 0; i < 3; i++) {
        if (this.id === `card${i}`) {
            console.log('oi')
            thisTurnCards.splice(i, 1);
        }
        lawCardFrame[i].remove();
    }

    for (let i = 0; i < 2; i++) {
        pileOfDiscard.push(thisTurnCards.pop());
    }
    
    threeCardsShow.remove();
    cancelChancellor();
    changePresident();
    reselectChancellor();
}